# python-packages-demo

Unofficial demo and howto for Python packages

## Popular packages
* [pypi.org](https://pypi.org):
  [python
  ](https://www.google.com/search?q=python+site%3Apypi.org)
  @ [Google
  ](https://www.google.com/search?q=site%3Apypi.org)
* [libraries.io/pypi](https://libraries.io/pypi):
  [python
  ](https://www.google.com/search?q=python+site%3Alibraries.io/pypi)
  @ [Google
  ](https://www.google.com/search?q=site%3Alibraries.io/pypi)
* [hugovk.github.io/top-pypi-packages
  ](https://hugovk.github.io/top-pypi-packages/)
* [packages.debian.org/sid/python](https://packages.debian.org/sid/python/):
  [python
  ](https://www.google.com/search?q=python+site%3Apackages.debian.org%2Fsid%2Fpython%2F)
  @ [Google
  ](https://www.google.com/search?q=site%3Apackages.debian.org%2Fsid%2Fpython%2F)
* @[Debian](https://qa.debian.org/developer.php?email=python-modules-team%40lists.alioth.debian.org)

### Some packages at Debian
* [python-decorator](https://tracker.debian.org/pkg/python-decorator)
* [traitlets](https://tracker.debian.org/pkg/traitlets)

## Dependency injection and Python
* [python dependency injection](https://www.google.com/search?q=python+dependency+injection)

## Python packages available in other languages
### [cssselect](https://pypi.org/project/cssselect/)
#### PHP
* [symfony](https://phppackages.org/s/symfony)/[css-selector](https://phppackages.org/p/symfony/css-selector)
